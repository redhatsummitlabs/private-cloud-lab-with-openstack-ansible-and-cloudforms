## Lab 2: Leverage multi-tenancy capabilities of Red Hat OpenStack Platform

In this lab you'll learn:
- How the use of OpenStack Projects enable multi-tenancy

### Overview of Multi-tenancy

In a multi-tenant environment, resources like compute, networking and storage are
isolated among different users. OpenStack implements this isolation through *projects*, allowing different organizations, customers or accounts, to use the same shared
environment. This is managed by the Identity Service, known by the project name
**keystone**.

Each project has its own configured quota for several kind of infrastructure resources,
like vCPUs, RAM, disk space, number of persistent volumes, floating IP addresses, etc.
This allows the administrator to restrict how much resources each project will use.

In order to have access to some resource, the user must have access to the project
where that resource belongs, unless the resource is public or shared, like some
networks or images.

Besides, users may have different roles. Default Red Hat OpenStack Platform roles
follows:

<table class="tg">
  <tr>
    <td class="tg-tcj5">admin<br></td>
    <td class="tg-0pky">OpenStack administrator with access to all
operations. Can manage projects and users.</td>
  </tr>
  <tr>
    <td class="tg-tcj5">_member_</td>
    <td class="tg-0pky">Regular user that can create/delete instances,
create/delete project networks, create/delete
volumes on projects he has access to.</td>
  </tr>
  <tr>
    <td class="tg-tcj5">heat_stack_user</td>
    <td class="tg-0pky">Regular user that can use heat orchestration
service to manage resources.</td>
  </tr>
</table>

Identity V3 included support to domains, which are high-level containers for projects,
users and groups. This allows administrators to configure roles for the user at the
project or at the domain level, enabling more fine grained Role Based Access Control
(RBAC) capabilities. For example, it's possible to configure an administrator that can
manage other users and projects for an specific domain.

It’s common to use the terms *project* and *tenant* interchangeably.

### Verify multi-tenancy

In this environment there’s a user which is associated to a project different than the
“test-drive” being used so far.

1. Login using the following credentials:

Login   | Password
--------|---------
redhat  | r3dh4t1!

2. Notice on the top right corner that this user sees only a project called **tenant2**.

![project_tenant2](images/project_tenant2.png)

3. Navigate to Project --> Compute --> Instances and start the existing *instance1*,
which although may have the same name of instances of other projects, is a
completely different one.

4. Access instance console and log into it using the username "*cirros*" and the
password "*cubswin:)*", without the quotes. This instance is using a lightweight
linux system called CirrOS, used mainly for tests.

5. Try pinging 8.8.8.8 or any other Internet public address and notice it won't work,
as this project doesn't have a virtual router connecting the private network to
the external one.

6. From OpenStack Dashboard, visualize Network Topology (Project --> Network -->
Network Topology). Note how different this is from the “test-drive” tenant.


[Previous (Lab 1)](lab1.md) | [Table of Contents](README.md#table-of-contents) | [Next (Lab 3)](lab3.md)
